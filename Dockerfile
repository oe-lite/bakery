FROM debian:11
LABEL maintainer="Esben Haabendal <esben@haabendal.dk>"

ARG DEBIAN_FRONTEND=noninteractive

# /bin/sh should be bash
RUN echo "dash dash/sh boolean false" | debconf-set-selections \
 && dpkg-reconfigure dash

# Install Debian packages
RUN apt-get update -qq -o Acquire::Check-Valid-Until=false \
 && apt-get install --no-install-recommends -qq -y --force-yes \
      sudo git lsb-release \
      python python-pip-whl \
      gcc python-dev libsqlite3-dev

# Install Python modules
RUN pip_wheel=$(ls /usr/share/python-wheels/pip-*.whl) \
 && python $pip_wheel/pip install \
      /usr/share/python-wheels/wheel-*.whl \
      /usr/share/python-wheels/setuptools-*.whl \
      /usr/share/python-wheels/pkg_resources-*.whl \
      /usr/share/python-wheels/pip-*.whl \
 && pip install \
      ply==3.11 \
      pysqlite==2.8.3

# Remove unneeded Debian packages and package littering
RUN apt-get remove --purge -q -y gcc python-dev libsqlite3-dev \
 && apt-get autoremove --purge -q -y \
 && apt-get clean \
 && rm -rf /var/lib/apt/lists/*
# For this to make sense, it needs to be squashed with the above RUN commands

# Install OE-lite bakery
ADD . /opt/bakery/
RUN cd /opt/bakery \
 && ./setup.py install

# Add non-root user and use it instead of root from here on
RUN useradd -m -s /bin/bash user \
 && echo user ALL=NOPASSWD: ALL > /etc/sudoers.d/user
USER user
